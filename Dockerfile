FROM resin/rpi-buildstep-armv6hf:latest

RUN apt-get install -y python unzip

RUN wget https://api.equinox.io/1/Applications/ap_pJSFC5wQYkAyI0FIVwKYs9h1hW/Updates/Asset/ngrok.zip?os=linux\&arch=arm\&channel=stable

RUN unzip ngrok.zip\?os\=linux\&arch\=arm\&channel\=stable 

RUN mv ngrok /bin/ngrok

ADD start /start

ADD index.html /index.html

CMD [/start]
